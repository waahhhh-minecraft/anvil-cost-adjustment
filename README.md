# Anvil cost adjustment

[[_TOC_]]

## Config

File: `anvil-cost-adjustment.json`

```json
{
  "debug": true,
  "breakable": true,
  "enchantmentLevels": 0,
  "renameLevels": 0,
  "repairLevels": 0,
  "repairItems": 0
}
```

### debug

- Default: `false`
- Description: Used for development. Lots of logs are written when set to `true`.

### breakable

- Default: `true`
- Description: Should the anvil be breakable? `true` is the native Minecraft behaviour.

### enchantmentLevels

- Default: `0`
- Description: How many levels should be consumed for enchantments? `0` means use native Minecraft behaviour.

### renameLevels

- Default: `0`
- Description: How many levels should be consumed for renaming? `0` means use native Minecraft behaviour.

### repairLevels

- Default: `0`
- Description: How many levels should be consumed for repair? `0` means use native Minecraft behaviour.

### repairItems

- Default: `0`
- Description: How many items should be consumed for repair? `0` means use native Minecraft behaviour.

## Development

### Prepare

#### Docker

```shell
docker run --rm -u gradle -v "$PWD":/home/gradle/project -w /home/gradle/project gradle gradle genSources
```

#### Gradle Wrapper

```shell
JAVA_HOME=~/.jdks/jbr-17.0.9/ ./gradlew genSources
```

### Build

Get the mod file from `build/libs`.

#### Docker

```shell
docker run --rm -u gradle -v "$PWD":/home/gradle/project -w /home/gradle/project gradle gradle build
```

#### Gradle Wrapper

```shell
JAVA_HOME=~/.jdks/jbr-17.0.9/ ./gradlew build
```
