package waahhhh.anvilcostadjustment;

import net.fabricmc.api.ModInitializer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import waahhhh.anvilcostadjustment.config.MyConfig;

public class AnvilCostAdjustment implements ModInitializer {
    public static final String MOD_ID = "anvil-cost-adjustment";

    public static final Logger LOGGER = LoggerFactory.getLogger(MOD_ID);

    public static final MyConfig CONFIG = MyConfig.createAndLoad();

    @Override
    public void onInitialize() {
        LOGGER.info("Anvil cost adjustment loaded");
        LOGGER.info(AnvilCostAdjustment.CONFIG.breakable() ? "true" : "false");
    }

    public static void debugInfo(String message) {
        if (!CONFIG.debug()) {
            return;
        }

        LOGGER.info(message);
    }
}
