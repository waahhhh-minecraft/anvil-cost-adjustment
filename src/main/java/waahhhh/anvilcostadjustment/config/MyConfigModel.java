package waahhhh.anvilcostadjustment.config;

import io.wispforest.owo.config.annotation.Config;
import io.wispforest.owo.config.annotation.RangeConstraint;
import waahhhh.anvilcostadjustment.AnvilCostAdjustment;

@Config(name = AnvilCostAdjustment.MOD_ID, wrapperName = "MyConfig")
public class MyConfigModel {
    public boolean debug = false;

    public boolean breakable = true;

    @RangeConstraint(min = 0, max = Integer.MAX_VALUE)
    public int enchantmentLevels = 0;

    @RangeConstraint(min = 0, max = Integer.MAX_VALUE)
    public int renameLevels = 0;

    @RangeConstraint(min = 0, max = Integer.MAX_VALUE)
    public int repairLevels = 0;

    @RangeConstraint(min = 0, max = Integer.MAX_VALUE)
    public int repairItems = 0;
}
