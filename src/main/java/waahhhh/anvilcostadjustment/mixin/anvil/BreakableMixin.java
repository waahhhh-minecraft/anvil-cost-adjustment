package waahhhh.anvilcostadjustment.mixin.anvil;

import net.minecraft.block.AnvilBlock;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import waahhhh.anvilcostadjustment.AnvilCostAdjustment;

@Mixin(AnvilBlock.class)
public class BreakableMixin {
    private static final String CLASS_ID = "BreakableMixin";

    @Inject(method = "getLandingState(Lnet/minecraft/block/BlockState;)Lnet/minecraft/block/BlockState;", at = @At("RETURN"), cancellable = true)
    private static void onBreakable(BlockState fallingState, CallbackInfoReturnable<BlockState> cir) {
        AnvilCostAdjustment.debugInfo(CLASS_ID + " called onBreakable");

        if (AnvilCostAdjustment.CONFIG.breakable()) {
            AnvilCostAdjustment.debugInfo(CLASS_ID + " keep vanilla behaviour");
            return;
        }

        AnvilCostAdjustment.debugInfo(CLASS_ID + " anvil is unbreakable");

        cir.setReturnValue(
            Blocks.ANVIL.getDefaultState().with(AnvilBlock.FACING, fallingState.get(AnvilBlock.FACING))
        );
    }
}
