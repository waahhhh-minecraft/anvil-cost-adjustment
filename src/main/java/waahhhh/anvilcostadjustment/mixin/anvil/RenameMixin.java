package waahhhh.anvilcostadjustment.mixin.anvil;

import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.AnvilScreenHandler;
import net.minecraft.screen.ForgingScreenHandler;
import net.minecraft.screen.Property;
import net.minecraft.screen.ScreenHandlerContext;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.screen.slot.Slot;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import waahhhh.anvilcostadjustment.AnvilCostAdjustment;

@Mixin(AnvilScreenHandler.class)
abstract class RenameMixin extends ForgingScreenHandler {
    private static final String CLASS_ID = "RenameMixin";

    @Shadow
    private Property levelCost;

    @Inject(method = "updateResult()V", at = @At("TAIL"))
    private void onRename(CallbackInfo ci) {
        AnvilCostAdjustment.debugInfo(CLASS_ID + " called onRename");

        if (AnvilCostAdjustment.CONFIG.renameLevels() == 0) {
            AnvilCostAdjustment.debugInfo(CLASS_ID + " keep vanilla behaviour");
            return;
        }

        Slot inputSlot = this.getSlot(AnvilScreenHandler.INPUT_1_ID);

        if (!inputSlot.hasStack()) {
            AnvilCostAdjustment.debugInfo(CLASS_ID + " empty inputSlot");
            return;
        }

        ItemStack inputStack = inputSlot.getStack();

        if (inputStack.isEmpty()) {
            AnvilCostAdjustment.debugInfo(CLASS_ID + " empty inputStack");
            return;
        }

        Slot outputSlot = this.getSlot(AnvilScreenHandler.OUTPUT_ID);

        if (!outputSlot.hasStack()) {
            AnvilCostAdjustment.debugInfo(CLASS_ID + " empty outputSlot");
            return;
        }

        ItemStack outputStack = outputSlot.getStack();

        if (outputStack.isEmpty()) {
            AnvilCostAdjustment.debugInfo(CLASS_ID + " empty outputStack");
            return;
        }

        if (inputStack.getName().equals(outputStack.getName())) {
            AnvilCostAdjustment.debugInfo(CLASS_ID + " same name");
            return;
        }

        AnvilCostAdjustment.debugInfo(CLASS_ID + " executed onRename");
        this.levelCost.set(AnvilCostAdjustment.CONFIG.renameLevels());
    }

    /**
     * necessary to extend ForgingScreenHandler
     */
    public RenameMixin(@Nullable ScreenHandlerType<?> type, int syncId, PlayerInventory playerInventory, ScreenHandlerContext context) {
        super(type, syncId, playerInventory, context);
    }
}
