package waahhhh.anvilcostadjustment.mixin.anvil;

import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.screen.AnvilScreenHandler;
import net.minecraft.screen.ForgingScreenHandler;
import net.minecraft.screen.Property;
import net.minecraft.screen.ScreenHandlerContext;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.screen.slot.Slot;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import waahhhh.anvilcostadjustment.AnvilCostAdjustment;

@Mixin(AnvilScreenHandler.class)
abstract class RepairMixin extends ForgingScreenHandler {
    private static final String CLASS_ID = "RepairMixin";

    @Shadow
    private int repairItemUsage;

    @Shadow
    private Property levelCost;

    @Inject(method = "updateResult()V", at = @At("TAIL"))
    private void onRepair(CallbackInfo ci) {
        AnvilCostAdjustment.debugInfo(CLASS_ID + " called onRepair");

        if (AnvilCostAdjustment.CONFIG.repairLevels() == 0 && AnvilCostAdjustment.CONFIG.repairItems() == 0) {
            AnvilCostAdjustment.debugInfo(CLASS_ID + " keep vanilla behaviour");
            return;
        }

        Slot inputSlot1 = this.getSlot(AnvilScreenHandler.INPUT_1_ID);

        if (!inputSlot1.hasStack()) {
            AnvilCostAdjustment.debugInfo(CLASS_ID + " empty inputSlot1");
            return;
        }

        ItemStack inputStack1 = inputSlot1.getStack();

        if (inputStack1.isEmpty()) {
            AnvilCostAdjustment.debugInfo(CLASS_ID + " empty inputStack1");
            return;
        }

        Slot inputSlot2 = this.getSlot(AnvilScreenHandler.INPUT_2_ID);

        if (!inputSlot2.hasStack()) {
            AnvilCostAdjustment.debugInfo(CLASS_ID + " empty inputSlot2");
            return;
        }

        ItemStack inputStack2 = inputSlot2.getStack();

        if (inputStack2.isEmpty()) {
            AnvilCostAdjustment.debugInfo(CLASS_ID + " empty inputStack2");
            return;
        }

        if (inputStack2.getItem().equals(Items.ENCHANTED_BOOK)) {
            AnvilCostAdjustment.debugInfo(CLASS_ID + " inputStack2 equals ENCHANTED_BOOK");
            return;
        }

        if (this.levelCost.get() == 0) {
            AnvilCostAdjustment.debugInfo(CLASS_ID + " invalid repair");
            return;
        }

        if (AnvilCostAdjustment.CONFIG.repairLevels() != 0) {
            AnvilCostAdjustment.debugInfo(CLASS_ID + " executed onRepair repairLevels");

            this.levelCost.set(AnvilCostAdjustment.CONFIG.repairLevels());
        }

        if (AnvilCostAdjustment.CONFIG.repairItems() != 0) {
            AnvilCostAdjustment.debugInfo(CLASS_ID + " executed onRepair repairItems");

            this.repairItemUsage = AnvilCostAdjustment.CONFIG.repairItems();

            if (inputStack2.getCount() < AnvilCostAdjustment.CONFIG.repairItems()) {
                AnvilCostAdjustment.debugInfo(CLASS_ID + " removed output stack");

                // clear output
                Slot outputSlot = this.getSlot(AnvilScreenHandler.OUTPUT_ID);
                outputSlot.setStack(ItemStack.EMPTY);
                this.levelCost.set(0);
            }
        }
    }

    /**
     * necessary to extend ForgingScreenHandler
     */
    public RepairMixin(@Nullable ScreenHandlerType<?> type, int syncId, PlayerInventory playerInventory, ScreenHandlerContext context) {
        super(type, syncId, playerInventory, context);
    }
}
